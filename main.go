package main

import (
	"bufio"
	"encoding/binary"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
	"sync"
	"time"
)

type Config struct {
	dim          int32 // transformer dimension
	hiddenDim    int32 // for ffn layers
	layerCount   int32 // numbers of layers
	headCount    int32 // number of query heads
	kVHeadCount  int32 // number of key/value heads (can be < query heads because of multiquery)
	vocabSize    int32 // vocabulary size, usually 256 (byte-level)
	maxSeqLength int32 // max sequence length
}

type TransformerWeights struct {
	// token embedding table
	tokenEmbeddingTable []float32 // (vocab_size, dim)
	rmsAttWeight        []float32 // (layer, dim) rmsnorm weights
	rmsFfnWeight        []float32 // (layer, dim)
	// weights for matmuls
	wq []float32 // (layer, dim, dim)
	wk []float32 // (layer, dim, dim)
	wv []float32 // (layer, dim, dim)
	wo []float32 // (layer, dim, dim)
	//weights for ffn
	w1 []float32 // (layer, hidden_dim, dim)
	w2 []float32 // (layer, dim, hidden_dim)
	w3 []float32 // (layer, hidden_dim, dim)
	// final rmsnorm
	rmsFinalWeight []float32 // (dim,)
	//freq_cis for RoPE relatively positional embeddings
	freqCisReal []float32 // (seq_len, dim/2)
	freqCisImag []float32 // (seq_len, dim/2)
	// (optional) classifier weights for the logits, on the last layer
	wcls []float32
}

type RunState struct {
	// current wave of activations
	x      []float32 // activations at current time stamp (dim,)
	xb     []float32 // same, but inside a residual branch (dim,)
	xb2    []float32 // an additional buffer just for convenience (dim,)
	hb     []float32 // buffer for hidden dimension in the ffn (hidden_dim,)
	hb2    []float32 // buffer for hidden dimension in the ffn (hidden_dim,)
	q      []float32 // query (dim,)
	k      []float32 // key (dim,)
	v      []float32 // value (dim,)
	att    []float32 // buffer for scores/attention values (n_heads, seq_len)
	logits []float32 // output logits
	// kv cache
	keyCache   []float32 // (layer, seq_len, dim)
	valueCache []float32 // (layer, seq_len, dim)
}

var (
	byteOrder = binary.LittleEndian
)

func main() {
	log.SetLevel(log.DebugLevel)
	/*
		temperature := 0.9
		steps := 256
	*/
	prompt := "this is a story about how I grew up and it got turned upside down"
	checkpoint := "llama2_7b.bin"

	start := time.Now()
	//open the checkpoint file
	checkpointFile, err := os.Open(checkpoint)
	if err != nil {
		log.Fatal("Unable to read checkpoint file: " + err.Error())
	}
	defer checkpointFile.Close()

	checkpointReader := bufio.NewReader(checkpointFile)

	var config Config
	readTo(checkpointReader, &config.dim)
	readTo(checkpointReader, &config.hiddenDim)
	readTo(checkpointReader, &config.layerCount)
	readTo(checkpointReader, &config.headCount)
	readTo(checkpointReader, &config.kVHeadCount)
	readTo(checkpointReader, &config.vocabSize)
	readTo(checkpointReader, &config.maxSeqLength)

	headSize := config.dim / config.headCount
	isSharedWeights := false
	// negative vocab size is hacky way of signaling unshared weights. bit yikes.
	if config.vocabSize > 0 {
		isSharedWeights = true
	}
	config.vocabSize = Abs(config.vocabSize)

	weights := TransformerWeights{
		tokenEmbeddingTable: make([]float32, config.vocabSize*config.dim),
		rmsAttWeight:        make([]float32, config.layerCount*config.dim),
		wq:                  make([]float32, config.layerCount*config.dim*config.dim),
		wk:                  make([]float32, config.layerCount*config.dim*config.dim),
		wv:                  make([]float32, config.layerCount*config.dim*config.dim),
		wo:                  make([]float32, config.layerCount*config.dim*config.dim),
		rmsFfnWeight:        make([]float32, config.layerCount*config.dim),
		w1:                  make([]float32, config.layerCount*config.dim*config.hiddenDim),
		w2:                  make([]float32, config.layerCount*config.dim),
		w3:                  make([]float32, config.layerCount*config.dim),
		rmsFinalWeight:      make([]float32, config.dim),
		freqCisReal:         make([]float32, config.maxSeqLength*headSize/2),
		freqCisImag:         make([]float32, config.maxSeqLength*headSize/2),
	}

	weightsOffset, err := checkpointFile.Seek(0, io.SeekCurrent)
	if err != nil {
		log.Fatal(err)
	}

	var wg sync.WaitGroup
	wg.Add(20)

	rmsAttOffset := weightsOffset + arraySize(&weights.tokenEmbeddingTable)
	wqOffset := rmsAttOffset + arraySize(&weights.rmsAttWeight)
	wkOffset := wqOffset + arraySize(&weights.wq)
	wvOffset := wkOffset + arraySize(&weights.wk)
	woOffset := wvOffset + arraySize(&weights.wv)
	rmsFfnOffset := woOffset + arraySize(&weights.wo)
	w1Offset := rmsFfnOffset + arraySize(&weights.rmsFfnWeight)
	w2Offset := w1Offset + arraySize(&weights.w1)
	w3Offset := w2Offset + arraySize(&weights.w2)
	rmsFinalOffset := w3Offset + arraySize(&weights.w3)
	freqCisRealOffset := rmsFinalOffset + arraySize(&weights.rmsFinalWeight)
	freqCisImagOffset := freqCisRealOffset + arraySize(&weights.freqCisReal)

	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, weightsOffset, &weights.tokenEmbeddingTable)
		log.Debug("finished loading token embeddings in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, rmsAttOffset, &weights.rmsAttWeight)
		log.Debug("finished loading rms att weight in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, wqOffset, &weights.wq)
		log.Debug("finished loading wq in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, wkOffset, &weights.wk)
		log.Debug("finished loading wk in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, wvOffset, &weights.wv)
		log.Debug("finished loading wv in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, woOffset, &weights.wo)
		log.Debug("finished loading wo in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, rmsFfnOffset, &weights.rmsFfnWeight)
		log.Debug("finished loading rms ffn weight in ", time.Since(start))
	}()
	//This one takes the majority of the time to load, breaking it up into pieces

	chunkSize := arraySize(&weights.w1) / 8
	remainder := chunkSize % 8
	numChunks := 8

	w1Weights := make([][]float32, numChunks)

	for i := 0; i < numChunks; i++ {
		go func(i int) {
			defer wg.Done()

			w1Start := w1Offset + int64(i)*chunkSize
			size := chunkSize
			if i == numChunks-1 && remainder > 0 {
				size = remainder
			}
			w1Weights[1] = make([]float32, size)
			sectionReadTo(checkpointFile, w1Start, &w1Weights[i])
			log.Debug("finished loading w1 ", i+1, "/", numChunks, " in ", time.Since(start))
		}(i)
	}

	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, w2Offset, &weights.w2)
		log.Debug("finished loading w2 in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, w3Offset, &weights.w3)
		log.Debug("finished loading w3 in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, rmsFinalOffset, &weights.rmsFinalWeight)
		log.Debug("finished loading rms final weight in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, freqCisRealOffset, &weights.freqCisReal)
		log.Debug("finished loading freqCisReal in ", time.Since(start))
	}()
	go func() {
		defer wg.Done()
		sectionReadTo(checkpointFile, freqCisImagOffset, &weights.freqCisImag)
		log.Debug("finished loading freqCisImag in ", time.Since(start))
	}()

	if isSharedWeights {
		weights.wcls = weights.tokenEmbeddingTable
	} else {
		weights.wcls = make([]float32, config.vocabSize*config.dim)
		sectionReadTo(checkpointFile, freqCisImagOffset+arraySize(&weights.freqCisImag), &weights.wcls)
		log.Debug("finished loading wcls in ", time.Since(start))
	}

	wg.Wait()

	for i := 0; i < numChunks; i++ {
		weights.w1 = append(weights.w1, w1Weights[i]...)
	}

	//clear the temp weights to reclaim memory
	w1Weights = nil

	tokenizerStart := time.Now()
	tokenizer, err := NewTokenizer("tokenizer.bin", int(config.vocabSize))
	if err != nil {
		log.Fatal("Failed to create tokenizer: ", err)
	}
	log.Debug("tokenizer took ", time.Since(tokenizerStart))
	//	runState = NewRunState()

	//BPE encode
	bpeStart := time.Now()
	tokens := tokenizer.BpeEncode(prompt)
	log.Debug("bpe encoding took ", time.Since(bpeStart), " to encode ", len(tokens), " tokens")

	//	var vocab string
	//	var vocabScores int
	log.Info("total time to populate:", time.Since(start))

	//read in the number of bytes for the size of a config struct
	//populate the config struct
	//check if the vocab size is a negative number or not and set the shared_weights based on that
	//absolute the vocab size in the config

	//
}

func NewRunState(config Config) *RunState {
	//allocate the run state
	return &RunState{
		x:          make([]float32, config.dim),
		xb:         make([]float32, config.dim),
		xb2:        make([]float32, config.dim),
		hb:         make([]float32, config.hiddenDim),
		hb2:        make([]float32, config.hiddenDim),
		q:          make([]float32, config.dim),
		k:          make([]float32, config.dim),
		v:          make([]float32, config.dim),
		att:        make([]float32, config.headCount*config.maxSeqLength),
		logits:     make([]float32, config.vocabSize),
		keyCache:   make([]float32, config.layerCount*config.maxSeqLength*config.dim),
		valueCache: make([]float32, config.layerCount*config.maxSeqLength*config.dim),
	}
}

func Abs(x int32) int32 {
	if x < 0 {
		return -x
	}
	return x
}

func matmul(xout []float32, x []float32, w []float32, n int, d int) {
	for i := 0; i < d; i++ {
		var val float32
		for j := 0; j < n; j++ {
			val += w[i*n+j] * x[j]
		}
		xout[i] = val
	}
}

func readTo(reader io.Reader, to interface{}) {
	if err := binary.Read(reader, binary.LittleEndian, to); err != nil {
		log.Fatal("failed reading from file", err)
	}
}

func sectionReadTo(file *os.File, pos int64, to *[]float32) {
	size := int64(len(*to) * 4)
	r := io.NewSectionReader(file, pos, size)
	readTo(r, to)
}

func arraySize(array *[]float32) int64 {
	return int64(len(*array) * 4)
}

func strLookup(str string, vocab []string) int {
	for k, v := range vocab {
		if str == v {
			return k
		}
	}
	return -1
}
