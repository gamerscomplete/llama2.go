package main

import (
	"bufio"
	"errors"
	log "github.com/sirupsen/logrus"
	"os"
)

type Tokenizer struct {
	vocab       []string
	vocabScores []float32
}

func NewTokenizer(filePath string, vocabSize int) (*Tokenizer, error) {
	//open the checkpoint file
	tokenizerFile, err := os.Open(filePath)
	if err != nil {
		return nil, errors.New("unable to read checkpoint file: " + err.Error())
	}
	defer tokenizerFile.Close()

	tokenizerReader := bufio.NewReader(tokenizerFile)

	var maxTokenLength uint32
	readTo(tokenizerReader, &maxTokenLength)

	vocab := make([]string, vocabSize)
	vocabScores := make([]float32, vocabSize)

	var length int32
	for i := 0; i < vocabSize; i++ {
		readTo(tokenizerReader, &vocabScores[i])
		readTo(tokenizerReader, &length)
		data := make([]byte, length)
		readTo(tokenizerReader, &data)
		vocab[i] = string(data)
	}
	return &Tokenizer{
		vocab:       vocab,
		vocabScores: vocabScores,
	}, nil

}

// byte pair encoding (BPE) tokenizer, encodes strings into tokens so we can prompt
func (tokenizer *Tokenizer) BpeEncode(text string) []int {
	//  strBuffer := strings.Builder{}

	tokens := make([]int, len(text))
	nTokens := len(text)

	for k, v := range text {
		id := strLookup(string(v), tokenizer.vocab)
		if id == -1 {
			log.Fatal("Could not find character in vocab: ", v)
		}

		tokens[k] = id
	}

	// merge the best consecutive pair each iteration according to the scores in vocabScores
	for {
		bestID := -1
		bestScore := float32(-1e10)
		bestIdx := -1

		for i := 0; i < nTokens-1; i++ {
			//check if we can merge the pair of tokens (tokens[i] and tokens[i+1]
			/*          strBuffer.Reset()
			            strBuffer.WriteString(vocab[tokens[i]])
			            strBuffer.WriteString(vocab[tokens[i+1]])
			*/
			id := strLookup(tokenizer.vocab[tokens[i]]+tokenizer.vocab[tokens[i+1]], tokenizer.vocab)
			if id != -1 && tokenizer.vocabScores[id] > bestScore {
				//this merge pair exists in vocab. record its score and position
				bestScore = tokenizer.vocabScores[id]
				bestID = id
				bestIdx = i
			}
		}
		if bestIdx == -1 {
			//we couldnt find any more pairs to merge, so were done
			break
		}
		// merge the consecutive pair (bestIdx, bestIdx + 1) into new token bestId
		tokens[bestIdx] = bestID
		for i := bestIdx + 1; i < nTokens-1; i++ {
			tokens[i] = tokens[i+1]
		}
		nTokens--
	}
	return tokens[:nTokens]
}
